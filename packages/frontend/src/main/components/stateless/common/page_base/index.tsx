import { CacheProvider } from "@emotion/react"
import { cache } from "@emotion/css"
import React from "react"
import { Layout } from "./layout"

const ContextProviders: React.FC = ({ children }) => (
	<CacheProvider value={cache}>
		{children}
	</CacheProvider>
)

export const PageBase: React.FC = ({ children }) => (
	<ContextProviders>
		<Layout>
			{children}
		</Layout>
	</ContextProviders>
)
