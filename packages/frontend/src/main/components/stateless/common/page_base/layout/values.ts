export const grid_areas = {
	header: "header",
	content: "content",
	side: {
		left: "side_left",
		right: "side_right",
	},
	footer: "footer",
}
