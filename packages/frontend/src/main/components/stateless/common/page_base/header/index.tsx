import React from "react"
import styled from "@emotion/styled"
import { Views } from "../../../views"

const Root = styled(Views.Basic)`
	background-color: hsl(0, 0%, 80%);
`

export const Header: React.FC<{ className?: string }> = ({ className }) => (
	<Root className={className}>
		Header
	</Root>
)
